extends KinematicBody2D

const SPEED = 300.0
const JUMP_VELOCITY = -400.0

var max_health = 10
var health = 10
var current_gold = 0
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var velocity = Vector2()

onready var PlayerAnim = get_node("AnimationPlayer")
onready var sndJump = get_node("sounds/jump").stream
onready var sndWalk = get_node("sounds/walk").stream
onready var sndLand = get_node("sounds/land").stream
onready var sndDeath = get_node("sounds/death").stream
onready var sndDamage = get_node("sounds/damage").stream

func is_alive() -> bool:
	return health > 0

var is_grounded = true
func _physics_process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit(0)

	if not is_alive():
		return
		
	var direction = Input.get_axis("ui_left", "ui_right")
	
	if direction == -1:
		get_node("PlayerAnimation").flip_h = false
	elif direction == 1:
		get_node("PlayerAnimation").flip_h = true
		
	if Input.is_action_just_pressed("ui_backspace"):
		kill()
		return
	
	if is_on_floor():
		if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_up"):
			Global.play_new_sound(sndJump, get_node("sounds"))
			velocity.y = JUMP_VELOCITY
			is_grounded = false
			
	if not is_on_floor():
		velocity.y += gravity * 0.3
		if velocity.angle() < 0:
			PlayerAnim.play("Jump")
		else:
			PlayerAnim.play("Fall")

	if direction:
		velocity.x = direction * SPEED
		if is_on_floor():
			PlayerAnim.play("Run")
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		if is_on_floor():
			PlayerAnim.play("Idle")

	move_and_slide(velocity, Vector2.UP)
	
func push_back(direction):
	velocity.y = JUMP_VELOCITY

func death_callback():
	get_tree().change_scene("res://main.tscn")

func kill():
	var tweenDisapear = get_tree().create_tween()

	velocity = Vector2()
	PlayerAnim.play("death")
	Global.play_new_sound(sndDeath, self)
	tweenDisapear.tween_callback(self, "death_callback")

func damage(amount):
	Global.play_new_sound(sndDamage, self)
	health -= amount
	if not is_alive():
		kill()
	
func heal(amount):
	health += amount
	if health > max_health:
		health = max_health

func reward(amount):
	current_gold += amount
	if current_gold > Global.playerGold:
		Global.playerGold = current_gold
		Global.saveGame()

