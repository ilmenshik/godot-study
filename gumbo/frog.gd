extends KinematicBody2D


const SPEED = 50.0
const JUMP_VELOCITY = -400.0
const DAMAGE = 5
const REWARD_GOLD = 1

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var Cristal = preload("res://items/cherry.tscn")

var velocity = Vector2()
var chase = false
onready var player = get_node("../../Players/Player")
onready var FrogAnim = get_node("FrogSprite")
onready var sndDeath = get_node("sounds/death").stream

func _ready():
	FrogAnim.play("Idle")

func is_in_death() -> bool:
	return FrogAnim.animation == "Death"

func _physics_process(delta):
	if is_in_death():
		return
	
	if is_on_floor():
		if is_instance_valid(player):
			if chase:
				FrogAnim.play("Jump")
				var direction = (player.global_transform.origin - self.global_transform.origin).normalized().x
				FrogAnim.flip_h = direction > 0
				velocity.x = SPEED * direction
			else:
				FrogAnim.play("Idle")
				velocity.x = 0
	else:
		velocity.y += gravity * 0.2
		FrogAnim.play("Fall")

		
	move_and_slide(velocity, Vector2.UP)

func _on_PlayerDetection_body_entered(body):
	if not is_instance_valid(body):
		return

	if body.name == "Player":
		chase = true

func _on_PlayerDetection_body_shape_exited(body_rid, body, body_shape_index, local_shape_index):
	if not is_instance_valid(body):
		return

	if body.name == "Player":
		chase = false

func _on_PlayerDetectionDeath_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if is_in_death():
		return

	if not is_instance_valid(body):
		return

	if body.name == "Player":
		var direction = (player.global_transform.origin - self.global_transform.origin).normalized().x
		var tweenDisapear = get_tree().create_tween()
		var selfBody = get_node("FrogCollision").get_parent()

		FrogAnim.play("Death")
		Global.play_new_sound(sndDeath, get_node("sounds"))
		selfBody.collision_layer = 2
		selfBody.collision_mask = 0
		body.reward(REWARD_GOLD)
		body.push_back(direction)
		tweenDisapear.tween_property(self, "modulate:a", 0, 1)
		tweenDisapear.tween_callback(self, "death_callback")

func death_callback():
	var cherryTemp = Cristal.instance()
	cherryTemp.position = self.position - Vector2(0, 10)
	get_node("../../collectables/timed_collectables").add_child(cherryTemp)
	queue_free()


func _on_PlayerCollision_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if is_in_death():
		return

	if not is_instance_valid(body):
		return

	if body.name == "Player":
		var direction = (body.global_transform.origin - self.global_transform.origin).normalized().x
		body.damage(DAMAGE)
		if is_instance_valid(body):
			body.push_back(direction)
