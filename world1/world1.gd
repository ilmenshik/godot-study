extends Node2D

var rng = RandomNumberGenerator.new()
var Frog = preload("res://gumbo/frog.tscn")
var Crystal = preload("res://items/crystal.tscn")


const background_tracks = [
	preload("res://world1/sounds/track0.ogg"),
	preload("res://world1/sounds/track1.ogg"),
	preload("res://world1/sounds/track2.ogg"),
	preload("res://world1/sounds/track3.ogg"),
	preload("res://world1/sounds/track4.ogg"),
	preload("res://world1/sounds/track5.ogg"),
	preload("res://world1/sounds/track6.ogg"),
	preload("res://world1/sounds/track7.ogg"),
	preload("res://world1/sounds/track8.ogg"),
	preload("res://world1/sounds/track9.ogg"),
]

func _ready():
	rng.randomize()

	var randint = rng.randi_range(0, len(background_tracks) - 1)
	
	Global.backgroundMusicPlayer.stream = background_tracks[randint]
	Global.backgroundMusicPlayer.volume_db = -12
	Global.backgroundMusicPlayer.play()
	get_tree().get_root().add_child(Global.backgroundMusicPlayer)


func _on_Timer_timeout():
	rng.randomize()

	var randint = rng.randi_range(64, 1280)
	var newTemp
	var parent

	if rng.randi_range(1, 10) < 3:
		newTemp = Crystal.instance()
		parent = get_node("Mobs")
	else:
		newTemp = Frog.instance()
		parent = get_node("collectables")

	newTemp.position = Vector2(randint, -10)
	parent.add_child(newTemp)



func _on_DeadZone_body_entered(body):
	if body.name == "Player":
		body.kill()
	else:
		body.queue_free()
