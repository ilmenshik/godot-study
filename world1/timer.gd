extends Timer


var Frog = preload("res://gumbo/frog.tscn")
var Crystal = preload("res://items/crystal.tscn")
var rng = RandomNumberGenerator.new()


func _on_Timer_timeout():

	var randint = rng.randi_range(64, 1280)
	var newTemp
	var parent

	if rng.randi_range(1, 10) > 3:
		newTemp = Crystal.instance()
		parent = get_node("../Mobs")
	else:
		newTemp = Frog.instance()
		parent = get_node("../collectables")
	
	newTemp.position = Vector2(randint, -10)
	parent.add_child(newTemp)
