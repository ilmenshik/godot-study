extends Area2D

const REWARD_HEALTH = 3

onready var CherryAnim = get_node("CherrySprite")
onready var sndPickup = get_node("sounds/pickup").stream

func _ready():
	CherryAnim.play("Idle")
	
func collect():
	queue_free()

func _on_Cherry_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if not is_instance_valid(body):
		return
	
	if body.name == "Player":
		body.heal(REWARD_HEALTH)
		CherryAnim.play("Feedback")
		Global.play_new_sound(sndPickup, get_node("sounds"))
		var tweenDisapear = get_tree().create_tween()
		tweenDisapear.tween_property(self, "modulate:a", 0, 0.3)
		tweenDisapear.tween_callback(self, "queue_free")
		
