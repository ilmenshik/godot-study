extends Node2D


var Frog = preload("res://gumbo/frog.tscn")
var rng = RandomNumberGenerator.new()


func _on_Timer_timeout():
	rng.randomize()
	var randint = rng.randi_range(64, 1280)
	var newTemp = Frog.instance()
	newTemp.position = Vector2(randint, -10)
	get_node("../Mobs").add_child(newTemp)
