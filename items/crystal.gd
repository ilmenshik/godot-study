extends KinematicBody2D

const REWARD_GOLD = 10

onready var CrystalAnim = get_node("CrystalSprite")
var velocity = Vector2()
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
onready var sndPickup = get_node("sounds/pickup").stream


func _ready():
	CrystalAnim.play("Idle")

func _physics_process(delta):
	velocity.y += gravity * 0.3
	move_and_slide(velocity, Vector2.UP)

	
func collect():
	queue_free()

func _on_PlayerDetection_body_entered(body):
	if not is_instance_valid(body):
		return
	
	if body.name == "Player":
		body.reward(REWARD_GOLD)
		CrystalAnim.play("Feedback")
		Global.play_new_sound(sndPickup, get_node("sounds"))
		var tweenDisapear = get_tree().create_tween()
		tweenDisapear.tween_property(self, "modulate:a", 0, 0.3)
		tweenDisapear.tween_callback(self, "queue_free")
