extends Node

const SAVE_PATH = "res://savegame.bin"
#const SAVE_PATH = "users://savegame.bin"
var antiflood = 0
var skip_menu = true
var playerHP = 10
var playerGold = 0

onready var backgroundMusicPlayer = AudioStreamPlayer.new()

func _ready():
	loadGame()
	get_tree().get_root().add_child(backgroundMusicPlayer)


func saveGame():
	var file = File.new()
	file.open(SAVE_PATH, File.WRITE)
	var data: Dictionary = {
		"playerHP": Global.playerHP,
		"playerGold": Global.playerGold,
		"skipMenu": skip_menu,
	}
	
	var jstr = JSON.print(data)
	file.store_line(jstr)
	

func loadGame():
	var file = File.new()
	if File.new().file_exists(SAVE_PATH):
		file.open(SAVE_PATH, File.READ)
		var json_text = file.get_as_text()
		file.close()
		var data = JSON.parse(json_text).result
		Global.playerHP = data.get('playerHP', playerHP)
		Global.playerGold = data.get('playerGold', playerGold)
		Global.skip_menu = data.get('skipMenu', false)


func antiflood(limit=5) -> bool:
	antiflood += 1
	if antiflood > limit:
		antiflood = 0
	if antiflood == 1:
		return true

	return false

func play_new_sound(sound: AudioStream, parent: Node = null):
	if not sound:
		return

	if not parent:
		get_tree().get_root()
	
	var stream = AudioStreamPlayer.new()
	stream.stream = sound
	stream.autoplay = true
	parent.add_child(stream)
	stream.play()
	stream.connect("finished", stream, "queue_free")
