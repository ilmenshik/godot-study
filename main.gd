extends Node2D

func _ready():
	if Global.skip_menu:
		Global.skip_menu = false
		_on_btnStart_pressed()
		
		
func _on_btnQuit_pressed():
	get_tree().quit(0)


func _on_btnStart_pressed():
	get_tree().change_scene("res://world1.tscn")


func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		_on_btnQuit_pressed()
	
	if Input.is_action_just_pressed("ui_accept"):
		_on_btnStart_pressed()
